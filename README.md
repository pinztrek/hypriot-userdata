# README #

Hypriot ( https://blog.hypriot.com ) is a very interesting distro for Raspberry Pi and similar SBC's (Odroid, etc.) which is designed to make it easy to stand up a docker environment. 

There are a some nuances to getting one working on wifi that I've worked through and am sharing to help others.

### What is this repository for? ###

* Hypriot uses cloud-init, which allows pre-staging of user, wifi, and even system data in the boot volume prior to first boot. 
* Once configured, some of these options can be difficult to change after the fact. Others, like wifi, can prevent connection to the pi.
* Cloud-init accepts parameters from a user-data file in the fat boot volume. 
* This example user-data is a bit more comprehensive than the examples provided on the hypriot or cloud-init sites and 
  is working in my testing. 

### How do I get set up? ###

* Download the hypriot distro from: https://blog.hypriot.com/downloads/
* Flash to a 4GB or larger uSD card. (I would recommend 8-16Gb minimum, docker stuff can get large)
* Mount the newly flashed uSD via a USB adapter or similar. There will be two volumes, one FAT, the other Ext4.
* Download the user-data file from this repository. You download directly or use "git clone https://bitbucket.org/pinztrek/hypriot-userdata". 
* After editing the downloaded user-data file for your needs, copy it to the boot volume. This will replace the one in the flash image.
Note: If at all possible edit the user-data file in a unix type editor. If you use notepad or wordpad on Windows it will use MS-DOS type linefeeds, which may break the processing. 
* Load the uSD card into your Pi, power up, and let it run for a bit. It can take 3-4 minutes for cloud-init to do it's thing, reboot, etc. 
* Eventually your pi will grab a DHCP address, and you can ssh into it. Use your password or if you loaded the authorized keys, you won't even be asked for password.
* There are many docker tutorials and downloadable docker packages. The hypriot site has a sample one that is the docker equivilant of "Hello World".
* Like any pi, you should review and tighten up security holes if you plan to use for more than learning. Prime example is disabling PAM and password logins once you have ssh cert login working. 

### Contribution guidelines ###

* You are welcome to use/contribute to this file. 
* Likewise the file can be distributed with no restrictions. 

### Who do I talk to? ###

* Feel free to contact myself with any input. 